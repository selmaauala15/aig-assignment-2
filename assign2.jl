### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ acb906b0-d020-11eb-1e25-53d0e77109b3
using Pkg;

# ╔═╡ c6fa0293-be4e-4535-9c03-cb52f62c6399
Pkg.activate("Project.toml")

# ╔═╡ 6defe33d-657f-4101-9c32-5a10d0cf1abd
using PlutoUI

# ╔═╡ 365d63fe-8705-4d35-81b7-71e3f9edd2db
using Flux

# ╔═╡ 403a6dcd-3c5f-492b-ba13-f16f83a4d79f
using Flux.Data: DataLoader

# ╔═╡ 55b1665d-b96d-4017-9f89-4bf7c15e02eb
using Flux: onehotbatch, onecold

# ╔═╡ cfe5117f-e635-4711-b041-c89404c37a82
using Flux.Losses: logitcrossentropy

# ╔═╡ 608e4f5d-377c-4c06-9c0f-8ce9fe9465e9
using Statistics, Random

# ╔═╡ e4a07ce3-5641-498d-9a7c-f6e29cefa55e
using Logging: with_logger

# ╔═╡ 41577557-eb84-47ff-af5f-a0b05e9700ea
using TensorBoardLogger: TBLogger, tb_overwrite, set_step!, set_step_increment!

# ╔═╡ 5a727da3-1cd7-4340-9cea-c2d8ada2119c
using ProgressMeter: @showprogress

# ╔═╡ ea94cb7c-93c8-4bee-a69c-42075b16841d
using MLDatasets

# ╔═╡ 49f393ca-793f-4e55-8099-8973ba41ed51
using Images

# ╔═╡ 71c11219-c49e-43df-9840-b5a69c225b0f
using CUDA

# ╔═╡ 0d2ee3b2-3a52-45cb-85c6-d9088e2d2032
import BSON

# ╔═╡ 51950e4d-6071-443f-9164-80ab35e87f9f
 md"# CNN LeNET5 Function "

# ╔═╡ c5b48b36-a88f-4bec-970a-d33699c75ec3
function CNN_LeNet5(; imgsize=(28,28,1), nclasses=10) 
    out_conv_size = (imgsize[1]÷4 - 3, imgsize[2]÷4 - 3, 16)
    
    return Chain(
            Conv((5, 5), imgsize[end]=>6, relu),
            MaxPool((2, 2)),
            Conv((5, 5), 6=>16, relu),
            MaxPool((2, 2)),
            flatten,
            Dense(prod(out_conv_size), 120, relu), 
            Dense(120, 84, relu), 
            Dense(84, nclasses)
          )
end

# ╔═╡ 3cc574d6-3040-411d-abe8-0d7bb9ea5f7b
global labels =["Normal","Pneumonia"]

# ╔═╡ 6a40abbb-39b6-4f10-87ab-998591545ccb
 md"#  Get Data Function"

# ╔═╡ b42b9cbe-ba06-462e-b483-e79c0bfc6987
function get_data(args)
	
	
    xtrain, ytrain = MLDatasets.MNIST.traindata(Float32, dir="./MNSIT_Dataset")
    xtest, ytest = MLDatasets.MNIST.testdata(Float32, dir="./MNSIT_Dataset")
	
	xtrain = reshape(xtrain, 28, 28, 1, :)
    xtest = reshape(xtest, 28, 28, 1, :)
 	l=0:9
	
    ytrain, ytest = onehotbatch(ytrain, l), onehotbatch(ytest, l)

    train_loader = DataLoader((xtrain, ytrain), batchsize=args.batchsize,shuffle=true)
    test_loader = DataLoader((xtest, ytest),  batchsize=args.batchsize)
    
    return train_loader, test_loader
end

# ╔═╡ 45f8683e-502a-49f5-88c4-ce1b6aa242b4
loss(ŷ, y) = logitcrossentropy(ŷ, y)

# ╔═╡ e9abdfde-d137-488c-979e-38faae915ce0
num_params(model) = sum(length, Flux.params(model)) 

# ╔═╡ af2cede6-03d0-46d8-8617-53caae890432
round4(x) = round(x, digits=4)

# ╔═╡ 160575b5-9e53-4c0c-956e-2e9feaa63064
function eval_loss_accuracy(loader, model, device)
    l = 0f0
    acc = 0
    ntot = 0
    for (x, y) in loader
        x, y = x |> device, y |> device
        ŷ = model(x)
        l += loss(ŷ, y) * size(x)[end]        
        acc += sum(onecold(ŷ |> cpu) .== onecold(y |> cpu))
        ntot += size(x)[end]
    end
    return (loss = l/ntot |> round4, acc = acc/ntot*100 |> round4)
end

# ╔═╡ e3662e87-c8f2-47fb-844c-0ac1bdf16462
 md"#  Args"

# ╔═╡ bd80dcfe-e006-4107-944d-a25eeb73221b
Base.@kwdef mutable struct args
    η = 3e-4             
    λ = 0   
	Use_Cuda = true      
    InfoTime = 1 	     
    savepath = "assignment/"
    batchsize = 128      
    Epochs = 10 
	CheckTime = 5        
    tblogger = true 
    Seed = 0             
     
end

# ╔═╡ 31f96796-f10a-4a3b-a7b5-6df7a91db8b9
 md"#  Train Function"

# ╔═╡ b206a648-3ae1-422c-afee-12b0e8023ccc
function train(; kws...)
    arguments = args(; kws...)
    arguments.Seed > 0 && Random.Seed!(arguments.Seed)
    Use_Cuda = arguments.Use_Cuda && CUDA.functional()
    
    if Use_Cuda
        Device = gpu
        @info "Training on GPU"
    else
        Device = cpu
        @info "Training on CPU"
    end

    ## DATA
    train_loader, test_loader = get_data(arguments)
    @info "Custom MNSIT Dataset Loaded"

    ## MODEL AND OPTIMIZER
    model = CNN_LeNet5() |> Device
    @info "LeNet5 model: $(num_params(model)) trainable params"    
    
    ps = Flux.params(model)  

    opt = ADAM(arguments.η) 
    if arguments.λ > 0 # add weight decay, equivalent to L2 regularization
        opt = Optimiser(WeightDecay(arguments.λ), opt)
    end
    
    ## LOGGING UTILITIES
    if arguments.tblogger 
        tblogger = TBLogger(arguments.savepath, tb_overwrite)
        set_step_increment!(tblogger, 0) # 0 auto increment since we manually set_step!
        @info "log File => \"$(arguments.savepath)\""
    end
    
    function report(epoch)
        train = eval_loss_accuracy(train_loader, model, Device)
        test = eval_loss_accuracy(test_loader, model, Device)        
        println("Epoch: $epoch   Train: $(train)   Test: $(test)")
        if arguments.tblogger
            set_step!(tblogger, epoch)
            with_logger(tblogger) do
                @info "train" loss=train.loss  acc=train.acc
                @info "test"  loss=test.loss   acc=test.acc
            end
        end
    end
    
    ## TRAINING
    @info "Start Training"
    report(0)
	
    for epoch in 1:arguments.Epochs
        @showprogress for (x, y) in train_loader
            x, y = x |> Device, y |> Device
            gs = Flux.gradient(ps) do
                    ŷ = model(x)
                    loss(ŷ, y)
                end

            Flux.Optimise.update!(opt, ps, gs)
        end
        
        ## Printing and logging
        epoch % arguments.InfoTime == 0 && report(epoch)
        if arguments.CheckTime > 0 && epoch % arguments.CheckTime == 0
            !ispath(arguments.savepath) && mkpath(arguments.savepath)
            modelpath = joinpath(arguments.savepath, "model.bson") 
            let model = cpu(model) #return model to cpu before serialization
                BSON.@save modelpath model epoch
            end
            @info "Model saved in \"$(modelpath)\""
        end
    end
end

# ╔═╡ bb1b489f-0e59-4de0-a145-40416444faee
train()

# ╔═╡ Cell order:
# ╠═acb906b0-d020-11eb-1e25-53d0e77109b3
# ╠═c6fa0293-be4e-4535-9c03-cb52f62c6399
# ╠═6defe33d-657f-4101-9c32-5a10d0cf1abd
# ╠═365d63fe-8705-4d35-81b7-71e3f9edd2db
# ╠═403a6dcd-3c5f-492b-ba13-f16f83a4d79f
# ╠═55b1665d-b96d-4017-9f89-4bf7c15e02eb
# ╠═cfe5117f-e635-4711-b041-c89404c37a82
# ╠═608e4f5d-377c-4c06-9c0f-8ce9fe9465e9
# ╠═e4a07ce3-5641-498d-9a7c-f6e29cefa55e
# ╠═41577557-eb84-47ff-af5f-a0b05e9700ea
# ╠═5a727da3-1cd7-4340-9cea-c2d8ada2119c
# ╠═ea94cb7c-93c8-4bee-a69c-42075b16841d
# ╠═0d2ee3b2-3a52-45cb-85c6-d9088e2d2032
# ╠═49f393ca-793f-4e55-8099-8973ba41ed51
# ╠═71c11219-c49e-43df-9840-b5a69c225b0f
# ╠═51950e4d-6071-443f-9164-80ab35e87f9f
# ╠═c5b48b36-a88f-4bec-970a-d33699c75ec3
# ╠═3cc574d6-3040-411d-abe8-0d7bb9ea5f7b
# ╟─6a40abbb-39b6-4f10-87ab-998591545ccb
# ╠═b42b9cbe-ba06-462e-b483-e79c0bfc6987
# ╠═45f8683e-502a-49f5-88c4-ce1b6aa242b4
# ╠═160575b5-9e53-4c0c-956e-2e9feaa63064
# ╠═e9abdfde-d137-488c-979e-38faae915ce0
# ╠═af2cede6-03d0-46d8-8617-53caae890432
# ╟─e3662e87-c8f2-47fb-844c-0ac1bdf16462
# ╠═bd80dcfe-e006-4107-944d-a25eeb73221b
# ╟─31f96796-f10a-4a3b-a7b5-6df7a91db8b9
# ╠═b206a648-3ae1-422c-afee-12b0e8023ccc
# ╠═bb1b489f-0e59-4de0-a145-40416444faee
